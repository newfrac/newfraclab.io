# Newfrac Computational Platform

[NEWFRAC](https://www.newfrac.eu) is the first coordinated initiative in EU aiming at the systematic progress in the failure prediction in heterogeneous systems through a novel computational framework by integrating Finite Fracture Mechanics and Phase Field modelling strategies.

The _Newfrac Computational Platform_ collects the common computational tools and knowledge basis for the network. 

You will find here the progress of the work of the network and the related codes we develop to solve fracture mechanics problems.

 - [Overview of the network](network/codes.md)
 - [Codes developed within the network](network/codes.md)
 - [Fracture mechanics examples in `fenics/dolfinx`](https://newfrac.gitlab.io/fenicsx-fracture/)
 - [Course materials](network/courses.md)
 - [Overview of the network](network/network.md)
