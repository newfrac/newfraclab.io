# Courses

We will add here a description and links to the materials of the different schools

- [Newfrac Core school  Numerics](https://gitlab.com/newfrac/CORE-school/newfrac-core-numerics)
  
- [Newfrac '21 FEniCS day](https://newfrac.gitlab.io/newfrac-fenicsx-training/)
 
- [Git-Gitlab class for the Newfrac Network held on March 2021](https://gitlab.com/newfrac/common-tools/gitlab-course)
