# Overview of the network

| ESR   | Name                           | Where       | Coding in       | PF  |FFM    | Topic        | Supervisor   |
| :----: | :------------------------------ | :----------- | :--------------- | :---: | :----: | :------------ | :------------ |
| 1   | Karthik Ambikakumari Sanalkumar| US, Seville | fenics/abaqus   | yes          | yes   |Hybrid PF-FFM |    Vladislav Mantic          |
| 2   | Simone Sangaletti              | Seville     | fenics/abaqus   | yes          | no    |Microstructure Optimization              |Israel Garcia Garcia              |
| 3   | Sara Jiménez Alfaro            | Paris       | fenics/abaqus   | yes          | yes   |Fracture analysis of advanced layered materials| Dominique Leguillon    |
| 4   | Anatoli Mitrou                 | Porto       | abaqus          | yes  | yes   | Fracture of LFRP ultra-thin ply laminates in aeronautical applications            |  Pedro Camanho    |
| 5   | Camilla Zolesi                 | SU, Paris   | FEniCS          | yes          | no    |Nucleation and propagation of compressive cracks | Corrado Maurini   |  
| 6   | Angela Fajardo Lacave          | Robert Bosch| FEniCS/MATLAB | yes  | no    | Multi-scale modelling of fracture in injection moulded SFRPs | Fabian Welschinger |
| 7   | Amir Mohammad Mirzaei          | Polito     | Fenics/Abaqus/Mathematica | yes          | yes    |  Debonding           |   Pietro Cornetti     |
| 8   | Maxime Levy                    | Tel Aviv University    | Fenics/Abaqus | yes         | no   |Fracture in biological anisotropic hard tissues (human bones)              | Zohar Yosibash       |
| 9   | Zeng Liu                       | IMT Lucca     | fenics/abaqus | yes          | no    | Multi-field and multi-scale modeling of fracture for renewable energy applications  | Marco Paggi       |
| 10  | Francesco Vicentini            | ETH Zürich      | Matlab, Fortran  | yes          | no    |PF modeling of fracture in the human femur             |Laura De Lorenzis        |
| 11  | Sindhu BUSHPALLI SHIVAREDDY    | FIDAMC      | abaqus          | yes          | yes    |  Analysis of failure mechanisms associated to the unfolding failure in CFRP Profiles           | Enrique Graciani       |
| 12  | Juan Macías                    | Porto     | fenics/abaqus     | yes    | yes    | Fracture in fibre-reinforced thermoplastics across scales            | Pedro Camanho  |
| 13  | Arturo Chao Correas            | POLITO      | FeniCS/Abaqus/ Matlab/Mathematica | yes          | yes    |Hybrid PF-FFM approaches for dynamic fracture in brittle materials  | Dr Mauro Corrado       |



