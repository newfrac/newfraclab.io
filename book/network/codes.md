# Codes

(codes:damage-dolfinx)=
## Gradient damage models with dolfinx
*Brief description*: This code is an object-oriented fenics-x applications aimed to provide basic tools to solve gradient-damage models. The model provide many utilities functions, as specific solvers and material models, and examples to solve gradient damage models to be used as phase-field approximation of brittle fracture
- *Name:* `damage-dolfinx`
- *Link:* https://gitlab.com/dalembert-mises/damage/damage-dolfinx
- *Access level*: 
  - [ ] public 
  - [x] on request (mailto: corrado.maurini@sorbonne-universite.fr)
  - [ ] confidential
- *Developers*: 
  - Camilla Zolesi, ESR-5 (camilla.zolesi@sorbonne-universite.fr)
  - Corrado Maurini (corrado.maurini@sorbonne-universite.fr)
  - Andres Alessandro Leon Baldelli (leon.baldelli@cnrs.fr)
  - Zakia Karoui (zakia.karoui@onera.fr)
-  *Current state*
   - [x] under active developement
   - [x] with ready functionalities 

## Building scripts for dolfinx 
*Brief description*: These scripts constitute a base to building the dolfinx toolchain on your computer or on clusters
- *Name:* `dolfinx-build`
- *Link:* https://gitlab.com/newfrac/public/dolfinx-build
- *Access level*: 
  - [x] public 
  - [ ] on request (mailto: corrado.maurini@sorbonne-universite.fr)
  - [ ] confidential
- *Developers*: 
  - Corrado Maurini (corrado.maurini@sorbonne-universite.fr)
  - Andres Alessandro Leon Baldelli (leon.baldelli@cnrs.fr)
-  *Current state*
   - [x] under active developement
   - [x] with ready functionalities 
## Automatization of the Coupled Criterion

*Brief description*: This code is used to automatically find the critical force and critical displacement for crack nucleation considering Finite Fracture Mechanics, in particular, the Coupled Criterion. 
- *Name:* `Automatization of the Coupled Criterion`
- *Link:* https://gitlab.com/newfrac/esrs/esr-3/coupled-criterion
- *Access level*: 
  - [ ] public 
  - [x] on request (mailto: sara.jimenez_alfaro@sorbonne-universite.fr)
  - [ ] confidential
- *Developers*: 
  - Sara Jiménez Alfaro, ESR-03 (sara.jimenez_alfaro@sorbonne-universite.fr)
  - Dominique Leguillon
-  *Current state* 
   - [x] under active developement
   - [x] with ready functionalities

## Phase field modeling for enhanced assumed strain solid shell and threee-dimensional large deformation interface element 
*Brief description*: This code implemented the phase field approach for solid shell element considering the enhanced assumed strain method and assumed natural strain method to alleviate locking effects, and also the three-dimensional large deformation interface element considering both geometrical and material nonlinearity into the finite element code Abaqus.
- *Name:* `Three-dimensional fracture modeling approach`
- *Link:* 
- *Access level*: 
  - [ ] public 
  - [x] on request (mailto: zeng.liu@imtlucca.it)
  - [ ] confidential
- *Developers*: 
  - Zeng Liu, ESR-9 (zeng.liu@imtlucca.it)
  - Marco Paggi (marco.paggi@imtlucca.it)
  - José Reinoso (jreinoso@us.es)
-  *Current state*
   - [x] under active developement
   - [x] with ready functionalities 

## Baseline implementation of the G-Theta and J-Integral methods for the obtention of the SERR under plane and axisymmetric conditions
*Brief description*: This repository contains the baseline pieces of coding used for the determination of the Strain Energy Release Rate (SERR) in plane and axisymmetric elastic problems through the G-Theta and the J-Integral methods. The latter was implemented in its domain integral variant for increased accuracy.

- *Name:* `serr-baseline`
- *Link:* https://gitlab.com/newfrac/esrs/esr-13/serr-baseline 
- *Access level*: 
  - [ ] public 
  - [x] on request (mailto: arturo.chaocorreas@polito.it)
  - [ ] confidential
- *Developers*: 
  - Arturo Chao Correas, ESR-13 (arturo.chaocorreas@polito.it);
  Based on codings by: Corrado Maurini (corrado.maurini@sorbonne-universite.fr)
-  *Current state* (Tick what is appropiate and/or add other possible states)
   - [ ] under active developement
   - [ ] with ready functionalities
   - [x] finished

## One-dimensional phase-field model for heterogeneous materials

*Brief description*: This MATLAB code solves the one-dimensional phase-field model for heterogeneous materials.

- *Name:* `PF-1D`
- *Link:* 
- *Access level*: (Tick one of the followings)
  - [ ] public 
  - [x] on request (mailto: fvicentini@ethz.ch)
  - [ ] confidential
- *Developers*: 
  - Francesco Vicentini, ESR-10 (fvicentini@ethz.ch)
-  *Current state* (Tick what is appropiate and/or add other possible states)
   - [ ] under active developement
   - [x] with ready functionalities


## Implementation of Principle of Minimum Total Energy with a discontinuous representation of cracks subject to a Stress Condition (PMTE-SC) criterion using UINTER

*Brief description*: Principle of Minimum Total Energy with a discontinuous representation of cracks subject to a Stress Condition (PMTE-SC) is implemented into the FE package ABAQUS through the user-defined subroutine UINTER, a user-defined constitutive model defining the interaction between the surfaces. 

- *Name:* `UINTER_PMTE-SC formulation`
- *Link:* Update soon...
- *Access level*: 
  - [ ] public 
  - [x] on request (mailto: karthik@us.es)
  - [ ] confidential
- *Developers*: 
  - Karthik Ambikakumari Sanalkumar, ESR-01 
  - M. Muñoz-Reja
  - Luis Távara
  - V. Mantič
-  *Current state* 
   - [x] under active developement
   - [x] with ready functionalities

   

## Automated crack advancement and detection using SEAM tool in ABAQUS by python scripts

*Brief description*: This python code helps to detect crack as well as create crack advancement using SEAM tool in ABAQUS and generates corresponding meshes in each time steps.

- *Name:* `SEAM_ABAQUS_PY`
- *Link:* Update soon...
- *Access level*: 
  - [ ] public 
  - [x] on request (mailto: karthik@us.es)
  - [ ] confidential
- *Developers*: 
  - Karthik Ambikakumari Sanalkumar, ESR-01 
  - V. Mantič
-  *Current state* 
   - [x] under active developement
   - [x] with ready functionalities 

## Implementation of Phase Field Model for fracture in human long bones 

*Brief description*: CT scan-based Finite Element Analysis on human bone (humerus) associated with Phase Field Model implemented by Camilla Zolesi (ESR-05) and Prof. Corrado Maurini. 
- *Name:* `PFM_humerus_frac`
- *Link:* 
- *Access level*:
  - [ ] public 
  - [x] on request (mailto: maximelevy@mail.tau.ac.il)
  - [ ] confidential
- *Developers*: 
  - Maxime Levy, ESR-08 (maximelevy@mail.tau.ac.il)
  - Corrado Maurini (corrado.maurini@sorbonne-universite.fr)
  - Camilla Zolesi, ESR-05 (camilla.zolesi@sorbonne-universite.fr)
-  *Current state* 
   - [x] under active developement
   - [] with ready functionalities

   
     
## Your code

Copy and edit this section to add the description of your code. YOu can then add a link to this section as [this one](codes:damage-dolfinx) on your personal ESR page.
- *Name:* `your-short-code-name`
- *Link:* Put here the link to the repository
- *Access level*: (Tick one of the followings)
  - [ ] public 
  - [x] on request (mailto: corrado.maurini@sorbonne-universite.fr)
  - [ ] confidential
- *Developers*: 
  - Your name, ESR-XX (your-mail)
  - Add any other developers
-  *Current state* (Tick what is appropiate and/or add other possible states)
   - [x] under active developement
   - [x] with ready functionalities
