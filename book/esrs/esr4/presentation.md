# ESR 4

**Anatoli Mitrou**

## Short biography

In 2020 I got my Master's (Integrated) in Mechanical Engineering and Aeronautics from the Uniersity of Patras (Greece), Department of Mechanical Engineering and Aeronautics, with my thesis focusing on progressive damage analysis in fatigue. During the final years of my studies I interned at Adamant composites Ltd. where conceptual studies for large deployable antennae were done, and twice at NASA's Langley Research Center, which included work on aeroelastic analysis of hypersonic engine components and PDA for structural adhesive joints. So, as one can understand, lots of finite element analysis.

```{figure} LRC.jpg
:name: label4
:height: 200px
Under the meatball at LaRC
````

I have now joined the NewFrac network as a PhD student at the Univsrsity of Porto, Faculty of Engineering (FEUP), where I will work on understadning how ultra-thin ply composites break, and imporve modeling techniques for that.

## Ph.D. thesis subject

**Fracture of LFRP ultra-thin ply laminates in aeronautical applications**

The project aims at the characterization of the structural and fracture behavior of ultra-thin ply composites and extension of modeling techniques for their fracture based on the Finite Fracture Mechanics (FFM) and Phase Field (PF) approcahes.
 
## Codes

Implementation Finite Fracture Mechanics criterions and Phase Field modeling approach.



