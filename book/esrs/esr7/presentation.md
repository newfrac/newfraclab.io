# ESR 7

Author: Amir Mohammad Mirzaei


## Short biography

Hey! I'm Amir Mohmmad, a "researcher" in Fracture Mechanics from Iran, who am a PhD candidate at Politechnico di Torino, Italy :)

I did my master's in Solid Mechanics at Iran University of Science and Technology and my bachelor's in Mechanical Engineering at the University of Tabriz, Iran. During my master's, I worked in an excellent center for experimental solid mechanics, although my focus was on analytical solutions (asymptotic solutions), which resulted in 8 Q1 international papers in analytical, numerical, and experimental fracture mechanics, during 2 years. My bachelor's thesis was analytical fracture analysis of piezoelectric materials using distributed dislocation technique, which I have two international papers, in this topic. I am now working on debonding analysis of composites using cohesive zone models, and classical Fracture Mechanics approaches. Another topic that I am working is fatigue failure analysis using classical Fracture Mechanics approaches and a numerical approach called Phase field.

Some of my technical skills are:

1	Mathematical modeling by using Asymptotic expansion method, Cohesive Zone Modeling and Distributed Dislocation Technique.

2	Determining stress field and stress intensity factors of cracks and notches by using finite element analysis and digital image correlation.

3	Subroutine in ABAQUS.

4	Experimentation.

5	Research and Scientific Writing.



You can also find me on GoogleScholar and ResearchGate via:

https://scholar.google.com/citations?user=bPey-okAAAAJ&hl=en

https://www.researchgate.net/profile/Amir-Mirzaei-5

![photo_2019-07-26_23-50-37](./photo_2021-04-26_19-28-44.jpg)

## Ph.D. thesis subject

**Debonding of the reinforcement in Fiber Reinforced Polymer and Fiber Reinforced Cementitious Matrix externally strengthened beams**

The main expected result of my project is a deeper understanding of the edge debonding and intermediate-crack induced debonding mechanisms, gathering information about how to design the external reinforcement of beams to prevent failure. The study is expected to clarify the essential differences in behavior between joints with flat and curved substrates and between different types of reinforcements during debonding. Several approaches might be used for such a goal, like Finite Fracture Mechanics (FFM), Cohesive Zone Modeling (CZM), and Phase Field (PF), using analytical or numerical techniques. Moreover, Quasi-static loading condition will be assumed at the first stage; then, the investigation will also consider cyclic and fatigue loadings. The analyses are expected to model external reinforcements made of Fiber Reinforced Polymers (FRP), Fiber Reinforced Cementitious Matrix (FRCM) composites (with either Carbon or PBO fibers), and Steel Reinforced Grout (SRG). It is important to observe that the debonding behavior of traditional FRP plates differs from the one shown by FRCM or SRG, since debonding occurs within the substrate or at matrix/substrate interface for FRP, while it is mainly governed by the bond between the fiber net and the cement based matrix for FRCM and SRG. In the latter case, the FFM and CCM models have to be improved by taking friction into account. While the investigation mainly focuses on the debonding of external reinforcements, some outcomes are expected to have a general feature and be interesting for all ERSs working on FFM, CZM and PF. Theoretical insights are expected for tackling friction and residual stresses, competition and interaction among different crack onset points, fatigue, and cyclic loadings.

**Primary results**

You can find the results of my recent publication that are validated against experimental data.
The goal of the investigation is to determine the load during debonding in direct shear tests according to three different cohesive zone laws and one finite fracture mechanics approach, along with the two most relevant design quantities, i.e., the maximum transferable load and the effective bond length.




## Journal Publications

1.	R. Bagheri, A. M. Mirzaei “Fracture Analysis in an Imperfect FGM Orthotropic Strip Bonded between Two Magneto-Electro-Elastic Layers”, Iranian Journal of Science and Technology: Transactions of Mechanical Engineering, Volume 43, Issue 2, June 2019, Pages 253–271.

2.	B. Bahrami, M. R. Ayatollahi, A. M. Mirzaei “Analysis of stresses and displacements in the vicinity of blunt V-notches by considering higher order terms”, Fatigue & Fracture of Engineering Materials & Structures, Volume 42, Issue 8, August 2019, Pages 1760-1774.

3.	A. M. Mirzaei, M. R. Ayatollahi, B. Bahrami “Asymptotic stress field and the coefficients of singular and higher order terms for V-notches with end holes under mixed-mode loading”, International Journal of Solids and Structures, Volume 172, November 2019, Pages 51-69.

4.	M. R. Ayatollahi, B. Bahrami, A. M. Mirzaei, M.Y. Yahya “Effects of support friction on mode I stress intensity factor and fracture toughness in SENB testing”, Theoretical and Applied Fracture Mechanics, Volume 103, October 2019, Page 102288.

5.	B. Bahrami, M. R. Ayatollahi, A. M. Mirzaei, F. Berto “Improved stress and displacement fields around V-notches with end holes”, Engineering Fracture Mechanics, Volume 217, August 2019, Page 106539.

6.	R. Bagheri, M. Ayatollahi, A. M. Mirzaei, M. M. Monfared “Multiple defects in a piezoelectric half-plane under electro-elastic in-plane loadings”, Theoretical and Applied Fracture Mechanics, Volume 103, October 2019, Page 102316.

7.	A. M. Mirzaei, M. R. Ayatollahi, B. Bahrami, F. Berto “Elastic stress analysis of blunt V-notches under mixed mode loading by considering higher order terms”, Applied Mathematical Modelling, Volume 78, February 2020, Pages 655-684.

8.	B. Bahrami, M. R. Ayatollahi, A. M. Mirzaei, M.Y. Yahya “Support type influence on rock fracture toughness measurement using semi-circular bending specimen”, Rock Mechanics and Rock Engineering, Volume 53, pages 2175–2183.

9.	A. M. Mirzaei, M. R. Ayatollahi, B. Bahrami, F. Berto “A new unified asymptotic stress field solution for blunt and sharp notches subjected to mixed mode loading”, International Journal of Mechanical Sciences,Volume 193, March 2021.

10.	A. M. Mirzaei, M. Corrado, A. Sapora, P. Cornetti, “Analytical Modeling of Debonding Mechanism for Long and Short Bond Lengths in Direct Shear Tests Accounting for Residual Strength.” Materials, Volume 14, November 2021.

