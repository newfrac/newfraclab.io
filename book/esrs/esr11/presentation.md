# ESR 11

Authors: Sindhu BUSHPALLI SHIVAREDDY


## Short biography


Hi! My name is Sindhu and I am from Bangalore, India. I hold a bachelor´s degree in Aeronautical engineering from VTU, India and a master´s degree in Aerospace engineering from ISAE-SUPAERO, Toulouse, France. Currently, I am pursuing my PhD at FIDAMC, Getafe and I am enrolled for doctoral program at University of Seville, Spain.

During my bachelor´s, I performed internships at India´s premiere organisations like Indian Institute of Science and Hindustan Aeronautics Limited. I am interested in gaining profuse knowledge about composites and it´s applications in aerospace sector and have been working on the same over the years. During my master´s, I completed my internship and research project on composite materials at AIRBUS, ISAE-SUPAERO and Institut Clement Ader(ICA), Toulouse. I have also interned in the composite manufacturing department of Indian Space Research Organisation(ISRO), India. 


```{figure} ../../esrs/esr11/Sindhu.png
:height: 200px
```  



## Ph.D. thesis subject

**Title**: Analysis of failure mechanisms associated to the unfolding failure in CFRP profiles


**Description**: Unfolding failure consists of a delamination produced in curved composite laminates when they are loaded under a bending moment which tries to open the curvature. This failure is typically associated to the interlaminar normal stress (INS) characterized by the interlaminar tensile strength (ILTS). The ILTS is generally obtained by a four-point bending test. 

The aim of this project is to analyse numerically and experimentally the failure mechanisms involving the unfolding failure in order to demonstrate a novel idea of unfolding failure associated to intralaminar stresses instead of the interlaminar stresses.

## Recent work

**Title**: Numerical study of unfolding failure using Phase Field fracture method

Abstract: Highly curved composite laminates used at the junction between two perpendicular panels experience unfolding failure. Unfolding failure is a delamination which occurs in the curved laminates when they are loaded under bending moment trying to flatten the curvature which induces out-of-plane stresses, causing different plies to separate. The interlaminar tensile strength is determined empirically by means of four point bending test of L-shaped unidirectional samples. Notwithstanding, many studies have noticed that four-point bending test, applied to curved multidirectional composite laminates, show a thickness dependance of interlaminar tensile strength with the thickness of the specimens. This thickness dependance is explained by the concept of induced unfolding (see, for instance, Gonzalez-Cantero [1]) which states that unfolding failure may be caused by both interlaminar and intralaminar stresses. In the first stage, an intralaminar crack is formed which, under a certain level of interlaminar stresses, propagates as a delamination. The current study is focused on analyzing the initial stage of unfolding failure mechanism, associated with the intralaminar stresses, using both numerical and experimental study.

Numerical analysis involves the study of failure mechanisms causing unfolding failure through finite element models in Abaqus using the available subroutines.  Numerical simulations include using Phase Field (PF) fracture method for determining intralaminar failure, Finite Fracture Mechanics (FFM) for debonding failure and to prove the instability of multiple delaminations after the initial intralaminar failure. Currently, Linear Elastic Brittle Interface Model (LEBIM) formulation in 3D is considered for the FFM approach [2]. For the PF approach, readily available subroutines (UMAT & HETVAL) with their different formulations [3,4] are being used for this research.

In the current work, PF fracture methodology is primarily studied by recreating simple 2D models in Abaqus and using the user material subroutine HETVALg available from the literature [2]. Later, several tests are being performed to understand the contribution of various parameters on the crack onset and its propagation. Following this, the 2D model was modified by adding weaker plies to contain the damage within the weaker ply and was monitored for the failure behavior. The same problem was recreated using a 3D model to analyze the propagation of damage through the width of specimen.

Furthermore, the potential of this phase field fracture method is being analyzed by performing simulations on various 2D models (including corner radius specimens) combining the phase field elements with regular plain strain elements. 

```{figure} ../../esrs/esr11/corner_radius.png
:height: 200px
```  

**References**

[1]		J.M. González-Cantero, E. Graciani, B. López-Romano, and F. París. 2018. Competing mechanisms in the unfolding failure in composite laminates. Compos Sci Technol, 156:223–230.

[2]	   M.Muñoz-Reja, L.Távara, V.Mantič, P.Cornetti. 2020. A numerical implementation of the Coupled Criterion of Finite Fracture Mechanics for elastic interfaces. Theor Appl Fract Mech, 108:102607.

[3]	   Y. Navidtehrani, C. Betegón, E. Martínez-Pañeda. 2021. A Unified Abaqus Implementation of the Phase Field Fracture Method Using Only a User Material Subroutine. Materials, 14(8):1913. 

[4]	   Y. Navidtehrani, C. Betegon, E. Martinez-Pañeda. 2021. A simple and robust Abaqus implementation of the phase field fracture method. Appl Eng Sci, 6:100050.

[5]	   E. Martinez-Pañeda. 2017. A control algorithm to overcome convergence problems in cohesive zone model analyses

[6]	   L.Garciá-Guzmán, J.Reinoso, A. Valverde-González, E. Martinez-Pañeda, L.Távara 2017. Numerical study of interface cracking in composite structures using a novel geometrically nonlinear Linear Elastic Brittle Interface Model: Mixed-mode fracture conditions and application to structured interfaces. compstruct.2020.112495


## Codes

- Currently working with HETVALg subroutine available from [3,4] and implementing control algorithm of [5,6] in corner radius specimen to capture the delamination propagation






