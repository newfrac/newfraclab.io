# ESR 3

Authors: Sara Jiménez Alfaro

## Short biography

Hello! My name is Sara Jiménez Alfaro, I am from Spain. I have studied Aeronautical Engineering (Bachelor+MSc) at University of Seville. The last year of my studied I was in Politecnico di Milano doing an Erasmus. Currently, I am doing my PhD thesis at Sorbonne Université, supervised by professor Dominique Leguillon and professor Corrado Maurini.

During my masters I was also working as an intern student in the Department of Continuum Mechanics and Structural Analysis at University of Seville.  Having discovered how interesting and exciting was doing research for me, I had in my mind to do a PhD, especially in the field of Fracture Mechanics. This was because, apart from the fact that it was the one I had been focused on, it was also the one I loved the most. My favourite characteristic about Fracture Mechanics is the influence that mathematics has in the behaviour of damage in solids. Something that can be initially thought as chaotic, is, however, explained by the harmony of equations.  Furthermore, I like the way FEM analysis is applied to get approximations of the real solutions to the models we use for describing the reality. 

NewFrac is, in my opinion, the best opportunity to do a PhD.  Participating in such programme gives me the opportunity to work with international students and professors. Furthermore, it could help me to learn how to work in different environments. Not only am I be doing a PhD in a very technically proficient environment, but also I am be working in a multicultural atmosphere, which would enrich me professionally and personally. 

```{figure} ../../esrs/esr3/sara_newfrac.png
:name: image
:height: 200px
```  

## Ph.D. thesis subject

Thesis: Fracture analysis of advanced layered ceramics

Ceramic materials are widely used in the industry owing to the countless advantages: oxidation and corrosion resistance, high-temperature stability, hardness and wear resistance. However, the main drawback of ceramics is their low fracture toughness, which is related to the spontaneous brittle failure of the component. In this context, advanced layered ceramics, sometimes bio-inspired, are designed to increase strength and energy absorption capacity (toughness). 

The principal aim of this project will regard the consistent extension of both Phase Field approach (PF) and Coupled Criterion within the Finite Fracture Mechanics framework (CCFFM) modeling, as modelling tools for predicting crack nucleation and growth in layered ceramics and thin films deposited on a substrate. This methodology will allow the robust analysis of the influence of micro- (flaws, grain sizes) and meso-structures (layers, stacking sequences) and will focus in addition on the role of residual stresses of various origins (thermal, chemical…) on the onset and growth of cracks.

Since both methods rely on a characteristic length, which should interact with the already mentioned sizes, it is expected to provide plausible explanations for relationships between the microstructure and fracture properties at the macro-scale. We want to pay special attention to natural structures such as mother-of-pearl, which has exceptional fracture properties. Extensions to other materials with a different microstructure: amorphous (polymers, glass), layered (ceramics, hybrid metal-ceramic composites), among other different typologies, will be considered.

## Codes

[Automatization of the Coupled Criterion](https://gitlab.com/newfrac/esrs/esr-3/coupled-criterion)

## PhD publications 

Jiménez-Alfaro, S. & Leguillon, D.  (2021).  Finite fracture mechanics at the micro-scale.application to bending tests of micro cantilever beams.Engineering Fracture Mechanics,18012. https://doi.org/10.1016/j.engfracmech.2021.108012

## Other publications

Jiménez-Alfaro, S., Mantič, V. FEM Benchmark Problems for Cracks with Spring Boundary Conditions Under Antiplane Shear Loadings. Aerotec. Missili Spaz. 99, 309–319 (2020). https://doi.org/10.1007/s42496-020-00068-w

Jiménez-Alfaro, S., Villalba, V. & Mantič, V. Singular elastic solutions in corners with spring boundary conditions under anti-plane shear. Int J Fract 223, 197–220 (2020). https://doi.org/10.1007/s10704-020-00443-5

Baranova, S., Mogilevskaya, S.G., Mantič, V. et al. Analysis of the Antiplane Problem with an Embedded Zero Thickness Layer Described by the Gurtin-Murdoch Model. J Elast 140, 171–195 (2020). https://doi.org/10.1007/s10659-020-09764-x
