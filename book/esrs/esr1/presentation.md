# ESR 1

**Karthik** Ambikakumari Sanalkumar

```{figure} Bewerbung_Foto_Karthik.jpeg
:name: label2
:height: 200px

```

## Short biography

I am a PhD student at NewFrac Innovative Training Network (ITN)- funded by the Marie Skłodowska- Curie Actions (MSCA), European Commission and also have the opportunity to do a double doctorate at the University of Seville and the IMT School for Advanced Studies Lucca.
Before coming to Spain, I worked as a research assistant in TU Berlin in collaboration with Fraunhofer IZM, where I acquired broad knowledge and experience in software development, especially on the topic of computer-aided geometry, 3D abstraction techniques, C++ (FEM libraries) etc.
	
I completed my Master in Computational Sciences in Engineering at TU Braunschweig. My studies involved primary research with Computer-aided simulation methods (especially FEM), crash analysis of mechanical structures, optimization methods and simulations using programming languages. Due to my special interest in fracture mechanics and numerical methods, the main focus of my final thesis is "Development of a suitable crash structure for effective absorption of crash energy and design of an effective load path within the battery pack". Previously, I received a Bachelor's degree in Mechanical Engineering from Kerala University, India.


## Ph.D. thesis subject
**Title:** Total energy minimization with stress conditions for mixed mode fracture in anisotropic heterogeneous materials and structures

**Objectives**

New computational methodologies based on the original formulations of CCFFM (w/discontinuous representation of cracks) and PF (w/regularized diffused representation of cracks) and the Principle of Minimum Total Energy subject to a Stress Condition will be developed and implemented in a FEM code to provide accurate and reliable predictions for complex mixed mode fracture problems involving simultaneous crack onset, propagation, and interactions with interfaces in anisotropic brittle materials.

New optimization procedures will be required, e.g., for CCFFM, a modified a staggered scheme aimed at global optimization in the feasible parameter-region given by the stress condition due to the efficiency of its computational implementation.

Novel forms of the degradation and local fracture energy functions will be proposed to impose stress conditions in PF. Staggered hybrid CCFFM and PF schemes, providing necessary information to the PF algorithm in terms of suitable boundary/interface conditions or prescribed values for PF damage variable in each time step, will be explored.

Validation of the computational tools developed by their application to industrial problems.

## Recent works
**Title:** Finite element implementation of CCFFM based on PMTE-SC to predict crack onset and growth in composites

```{figure} Picture1.png
:name: label1
:height: 300px

```

**Abstract**

The numerical prediction of fracture in composites is a classical challenge in both solid mechanics and computational science. In the past two decades, extensive research has been conducted to develop effective and accurate numerical models for simulating fracture behaviour. The main objective of this work is to present the numerical efforts for developing algorithms to predict crack onset and growth in composites. This research focuses on the simulation of the two-dimensional cracking process in brittle materials based on the Coupled Criterion of Finite Fracture Mechanics (CCFFM). In the framework of the Finite Fracture Mechanics (FFM), assuming crack advances by finite steps, in opposite to the hypothesis of crack advance by infinitesimal steps adopted in the classical Linear Elastic Fracture Mechanics (LEFM), Leguillon proposed the coupled criterion which requires both stress and energy conditions are simultaneously fulfilled [1]. 

In the present work, the prospective crack path is modelled by a discontinuous representation of crack bridged by a continuous distribution of stiff springs with a linear elastic behaviour up to its breakage, which is a similar approach as used previously in Linear Elastic-(perfectly) Brittle Interface Model (LEBIM) [2]. The methodology of the current work is the application of CCFFM to the spring model based on the Principle of Minimum Total Energy subjected to a Stress Condition (PMTE-SC), where the energy criterion is imposed by minimizing the total energy change due to a crack advance following. This new formulation of the CCFFM was introduced by Mantič [3], who analyzed several aspects of the application of the PMTE-SC to the crack onset and propagation in composites, assuming a quasi-static problem evolution, i.e., inertial forces are neglected. The CCFFM based on the PMTE-SC is more versatile than the approach based on the study of the energy criterion curves, which is the most used so far. For this reason, the implementation of the CCFFM by PMTE-SC in FEM would provide a tool capable of solving complex problems [2].
The FEM has been widely used to solve different problems in the field of fracture mechanics. Apart from the standard FEM along with remeshing and adaptive meshing techniques, new methods have been developed to improve the accuracy of the solution in 2D linear elastic fracture mechanics problems, in the last two decades, such as the extended finite element method (XFEM) or the phantom node method (PNM). However, these techniques require additional DOFs as well as enriched elements which results into problems with transition elements in the case of XFEM, whereas in the case of PNM, lack of crack tip functions causes non-asymptotic stress field in crack tip [4]. In addition, the implementation of CCFFM based on PMTE-SC in these recent techniques is a computationally intensive operation. Therefore, the simplest implementation is to simulate crack propagation along the element edges, in which the cracks are geometrically modelled as topological discontinuities, i.e., cracks are introduced explicitly during the discretization of the domain, matching the faces of the elements with the crack faces. In this work, we introduce the implementation within the FEM software ABAQUS using some of the subroutines such as UMAT, USDFLD etc…

**References**

[1]	D. Leguillon. 2002. Strength or toughness? a criterion for crack onset at a notch. European Journal of Mechanics A/Solids, 21, 61–72.

[2]	M. Muñoz-Reja, L. Távara, V. Mantič, P. Cornetti. 2016. Crack onset and propagation at fibre-matrix elastic interfaces under biaxial loading using finite fracture mechanics. Composites Part A, 82, 267–278.

[3]	V. Mantič. Prediction of initiation and growth of cracks in composites. 2014. Coupled stress and energy criterion of the finite fracture mechanics. 16th European Conference on Composite Materials (ECCM 2014).

[4]	M. Marco, D. Infante-García, R. Belda, E. Giner 2020. A comparison between some fracture modelling approaches in 2D LEFM using finite elements. International Journal of Fracture, 223(1), 151-171.


## Codes

- Implementation of Principle of Minimum Total Energy with a discontinuous representation of cracks subject to a Stress Condition (PMTE-SC) criterion using UINTER

- [StressCrackGen](https://gitlab.com/newfrac/esrs/esr-1/stresscrackgen) - Cracks generated using Stress Criterion. StressCrackGen is a Python project developed to automate stress analysis and crack detection in finite element models. The project utilises various algorithms to identify potential crack regions based on stress criteria and generate crack paths accordingly.

- [Automated crack advancement and detection using SEAM tool in ABAQUS by python scripts](codes:SEAM_ABAQUS_PY)


## Publications and Conferences

A) Uerlich Roland, **Karthik Ambikakumari Sanalkumar**, Tjorben Bokelmann, and Thomas Vietor. "Finite element analysis considering packaging efficiency of innovative battery pack designs." International Journal of Crashworthiness 25, no. 6 (2020): 664-679. [DOI: 10.1080/13588265.2019.1632545](https://doi.org/10.1080/13588265.2019.1632545)

B) Modified sauvola based segmentation and RGB channel correction for shadow removal in panchromatic images, Gopika J L and **Karthik Ambikakumari Sanalkumar**, August 2020, Conference: Conference: I-CONCISE 2020, International Conference Novel Changes in Systematic Electronics, [Link](https://www.researchgate.net/publication/344329452_Modified_sauvola_based_segmentation_and_RGB_channel_correction_for_shadow_removal_in_panchromatic_images/stats)


