# ESR 10

Authors: Francesco Vicentini

## Short biography

Hello! My name is Francesco Vicentini. I am a PhD student at ETH Zürich's Computational Mechanics Group (Zurich, Switzerland).

Previously, I obtained a master's degree in Mechanical Engineering from Politecnico di Milano (Milan, Italy). During master's studies, I was Eramsus student at Technische Universität Braunschweig (Braunschweig, Germany) and I developed a thesis at the ETH Zürich's Laboratory for Bone Biomechanics.

```{figure} ../../esrs/esr10/Photo.jpg
:width: 5cm
```

## Ph.D. thesis subject: Phase Field modeling of fracture in the human femur

In western world, fracture in the human femur is considered a major public health problem and the related health services costs are expected to continue their growth in the next future [1, 2, 3, 4]. 

Numerical frameworks for bone fracture prediction are an invaluable tool for prevention and to improve the efficiency of biomedical implants. The techniques are usually based on finite element method applied on images acquired through computed tomography [5]. 
Bones have a very complicated three-dimensional organization and their internal fractures follow intricate patterns. Therefore, computationally expensive techniques such as X-FEM have little applicability [6, 7]. Classical options are either adopting cohesive zone models on oversimplified problems with crack paths known a-priori [8, 9, 10] or application of highly efficient erosion algorithms based on overconservative damage criteria [11, 12, 13].

Phase-field modeling of brittle fracture, first proposed by Bourdin et al. [14] as regularization of Francfort and Marigo’s variational approach to fracture mechanics [15] and later re-interpreted as a special family of gradient damage models [16], provides a remarkably flexible variational framework to describe the nucleation and propagation of cracks with arbitrarily complex geometries and topologies in two and three dimensions. Therefore, it offers a novel promising framework for bone fracture as shown by Shen et al. [17].

From the theoretical point of view, one of the main difficulties in the predictive simulation of fracture in bones is the heterogeneity of the tissue. The phase-field approach was originally developed under the assumption that the brittle material exhibits homogeneous elastic and fracture properties (fracture toughness). Therefore, it requires an extension to the case of heterogeneous mechanical properties.

Previous studies addressing fracture in heterogenous materials have adopted a pragmatic approach, by simply substituting the constant fracture toughness of the original model with a fracture toughness depending on the material point [18, 19, 20]. However, the implications of heterogeneous material properties on the key predictions of the phase-field model have not been thoroughly investigated yet.

In this thesis, a suitable energy functional that accounts for heterogeneity is developed and a mathematical investigation is proposed in the one and two-dimensional settings revisiting the fundamental analysis in [16]. Finally, in collaboration with ESR8 the model will be validated and applied to three-dimensional bone images.

## Codes

I develop a code in MATLAB for the one-dimensional phase-field model for heterogeneous materials. You will find further information [here](codes:PF-1D).

## References

[1] Broderick, J. M., et al. "Osteoporotic hip fractures: the burden of fixation failure." The Scientific World Journal 2013 (2013).

[2] National Osteoporosis Foundation. Osteoporosis Fast Facts. 2015. url: https://cdn.nof.org/wp-content/uploads/2015/12/Osteoporosis-Fast-Facts.pdf (visited on 06/21/2021).

[3] Lindsay, Robert. "The burden of osteoporosis: cost." The American journal of medicine 98.2 (1995): 9S-11S. 

[4] Sternheim, Amir, et al. "Pathological fracture risk assessment in patients with femoral metastases using CT-based finite element methods. A retrospective clinical study." Bone 110 (2018): 215-220.

[5] Yosibash, Zohar, et al. "Predicting the stiffness and strength of human femurs with real metastatic tumors." Bone 69 (2014): 180-190.

[6] Budyn, Elisa, Thierry Hoc, and Julien Jonvaux. "Fracture strength assessment and aging signs detection in human cortical bone using an X-FEM multiple scale approach." Computational Mechanics 42.4 (2008): 579-591.

[7] Budyn, E., and Thierry Hoc. "Analysis of micro fracture in human Haversian cortical bone under transverse tension using extended physical imaging." International Journal for Numerical Methods in Engineering 82.8 (2010): 940-965.

[8] Ural, Ani, and Deepak Vashishth. "Effects of intracortical porosity on fracture toughness in aging human bone: a μ CT-based cohesive finite element study." (2007): 625-631.

[9] Tomar, Vikas. "Modeling of dynamic fracture and damage in two-dimensional trabecular bone microstructures using the cohesive finite element method." (2008): 021021.

[10] Ural, Ani. "Prediction of Colles’ fracture load in human radius using cohesive finite element modeling." Journal of Biomechanics 42.1 (2009): 22-28.

[11] Hambli, Ridha, Awad Bettamer, and Samir Allaoui. "Finite element prediction of proximal femur fracture pattern based on orthotropic behaviour law coupled to quasi-brittle damage." Medical engineering & physics 34.2 (2012): 202-210.

[12] Khor, Fiona, et al. "Importance of asymmetry and anisotropy in predicting cortical bone response and fracture using human body model femur in three-point bending and axial rotation." Journal of the mechanical behavior of biomedical materials 87 (2018): 213-229.

[13] Ridha, Hambli, and Philipp J. Thurner. "Finite element prediction with experimental validation of damage distribution in single trabeculae during three-point bending tests." Journal of the mechanical behavior of biomedical materials 27 (2013): 94-106.

[14] Francfort, Gilles A., and J-J. Marigo. "Revisiting brittle fracture as an energy minimization problem." Journal of the Mechanics and Physics of Solids 46.8 (1998): 1319-1342.

[15] Bourdin, Blaise, Gilles A. Francfort, and Jean-Jacques Marigo. "Numerical experiments in revisited brittle fracture." Journal of the Mechanics and Physics of Solids 48.4 (2000): 797-826.

[16] Pham, Kim, et al. "Gradient damage models and their use to approximate brittle fracture." International Journal of Damage Mechanics 20.4 (2011): 618-652. 

[17] Shen, Rilin, et al. "A novel phase field method for modeling the fracture of long bones." International journal for numerical methods in biomedical engineering 35.8 (2019): e3211.

[18] Natarajan, Sundararajan, Ratna K. Annabattula, and Emilio Martínez-Pañeda. "Phase field modelling of crack propagation in functionally graded materials." Composites Part B: Engineering 169 (2019): 239-248.

[19] Kumar, PK Asur Vijaya, et al. "Phase field modeling of fracture in Functionally Graded Materials: Γ-convergence and mechanical insight on the effect of grading." Thin-Walled Structures 159 (2021): 107234.

[20] Hossain, M. Z., et al. "Effective toughness of heterogeneous media." Journal of the Mechanics and Physics of Solids 71 (2014): 15-32.

