# ESR 6

Angela Fajardo Lacave


## Short biography

Hola! My name is Angela and I come from Sevilla, where I graduated in Industrial Technologies Engineering. I specialized in material mechanics. After my degree, I got the chance to participate in an Erasmus Mundus Joint Master Degree, issued by AIX Marseille University, Tor Vergata Roma and Politechnical University of Wroclaw. The academic program was focusing on nanoengineering and I got a different perspective about materials and their behaviour. My versatile background led me to this PhD position within NewFrac. I am currently working in Robert Bosch campus for research and advance engineering. Though set in Stuttgart, I am enrolled in ETH Zürich. My doctoral research focuses on the fracture behaviour of short fiber reinforced polymers.


## Ph.D. thesis subject

**Title:** Multiscale modeling of fracture processes in injection molded SFRPs



**Objectives**

With technological advancements of manufacturing processes and high demands for efficient part making, injection moulding is nowadays known as an effective method for the mass production of components made of short fibre reinforced plastics (SFRPs). The suitability of injection moulding is in light of its capability in forming complex geometries and delivering fairly resilient engineering parts, in a timely manner. However, injection moulded parts are highly affected by the orientation and positioning of fibres, in terms of local/global deformational behaviour and failure mechanisms. In other words, uneven distribution of short fibres in the moulded part may give rise to heterogeneity and anisotropy, the severity of which being increased as the fibre volume fraction grows. Moreover, the injection moulded plastics show some extent of non-linear material behaviour, i.e. inelasticity, which complicates the modelling of these materials.  Also, probable existence of impurities such as voids and gas traps not only play a role in heterogeneity but also in making the part more susceptible to fracture nucleation and growth. Since these parts are usually employed in key industries such as automotive, therefore suitable understanding of their fracturing event is essential to guarantee performance and safe operation. 

In this PhD project, we aim to fully account for three substantial characteristics of injection moulded SFRPs, especially in our numerical simulations: Anisotropy, Inhomogeneity and Inelasticity. The methodology selected to deal with this problem is based upon Phase Field (PF) approach and multiscale modelling techniques. These schemes are rather new methodologies still being actively developed by researchers worldwide, meanwhile they have attracted much attention in recent years. The industrial and academic supervisors responsible for this project are Dr Fabian Welschinger and Prof. Laura De Lorenzis, respectively affiliated with Robert Bosch GmbH and ETH Zürich. The aforementioned supervisors are highly qualified in Computational Solid Mechanics and particularly in the mentioned methodologies, therefore they are reliable sources in guiding me through my research path.


## Codes

To predict and model fracture in anisotropic materials, a phase field anisotropic model has been developed using Fortran. This model takes into account the anisotropy in both the elastic and fracture mechanical properties of the material. The FEM software Abaqus is utilized for implementing this model. Currently, the Phase Field AT1 model is employed through the penalization approach, with FEM being utilized in conjunction with the heat transfer phase field analogy, specifically for your research purposes.

## Personal pages

## References

[1] P. Carrara, M. Ambati, R. Alessi, and L. De Lorenzis, 2020, A framework to model the fatigue behavior of brittle materials based on a variational phase-field approach. Comp. Meth. App. Mech. Eng., 361, 112731. 

[2] Bourdin B., Francfort G. A., Marigo J.-J., 2000, Numerical experiments in revisited brittle fracture. Journal of the Mechanics and Physics of Solids, 48 (4): 797-826.

[3] FeelMath software Information on http://www.itwm.fraunhofer.de/feelmath, 2022, Fraunhofer

[4] Hessman, P. A.; Riedel, T.; Welschinger, F.; Hornberger, K.; Böhlke, T., 2019,  Microstructural analysis of short glass fiber reinforced thermoplastics based on x-ray micro-computed tomography. Composites Science and Technology, 183: 107752

[5] Köbler, J.; Schneider, M.; Ospald, F.; Andrä, H.; Müller, R., 2019, Fiber orientation interpolation for the multiscale analysis of short fiber reinforced composite parts. Computational Mechanics, 61: 729 - 750.

[6] Miehe, C.; Hofacker, M.; Welschinger, F., 2010,  A phase field model for rate-independent crack propagation: robust algorithmic implementation based on operator splits. Computer Methods in Applied Mechanics and Engineering, 199: 2765 – 2778.

[7] Miehe, C.; Welschinger, F.; Hofacker, M., 2010, Thermodynamically consistent phase field models of fracture: variational principles and multi-field FE implementations. International Journal for Numerical Methods in Engineering, 83: 1273 – 1311.

[8] Pham, K.; Amor, H.; Marigo, J.-J.; Maurini, C., 2011,  Gradient damage models and their use to approximate brittle fracture.  International Journal of Damage Mechanics 20 (4): 618-652.

[9] Schneider, M., 2017,  The sequential addition and migration method to generate representative volume elements for the homogenization of short fiber reinforced plastics.  Computational Mechanics, 59: 247 - 263.

[10] Seles, K.; Lesicar, T.; Tonkovic, Z.; Soric, J., 2019,  A residual control staggered solution scheme for the phase-field modeling of brittle fracture. Engineering Fracture Mechanics, 205: 370 – 386.


