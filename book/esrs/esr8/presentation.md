# ESR 8 

Authors: Maxime Levy



## Short biography


My name is Maxime Levy, I am from Paris. I obtained my bachelor degree of mechanical engineering at Sorbonne University after an internship at Institut Jean le Rond d'Alembert lab under the supervision of a PhD student. Then I obtained a Master double-degree of computational mechanics from Sorbonne University and Sapienza University of Rome after one exchange semester at Sapienza University. I also did my master thesis abroad, at the computational mechanics and experimental biomechanics lab of Tel Aviv University. My thesis was about the heterogeneous fracture toughness in long bones and a phase field numerical implementation of a case study with a heterogeneous fracture toughness using FeniCs. 
I have been interested by fracture mechanics since I discovered it in class but my first intention after the studies was to work in aeronautics. However the international experience and the experimental aspect of fracture mechanics, that was missing in my background, through the proposed Newfrac position really interested me and motivated me to apply. 

## Ph.D. thesis subject: Fracture in biological anisotropic hard tissues (human bones)

Bone fracture prediction with Finite Element Analysis has been studied in [1] based on maximum strain criterion and Computerized-Tomography (CT) images. The model can predict fracture load with maximum 20% conservativeness.  It cannot predict the crack path which is interesting for clinical prevention. Thus, we want to use Phase Field Method (PFM) to improve bone fracture prediction regarding the fracture load and the crack path. So far, we are focusing on human femoral cortex and its failure in transverse direction (perpendicular to osteons).  Cortical bone is a heterogeneous and transversely isotropic material. One of the key parameters of PFM is the fracture toughness or more precisely the critical Energy Release Rate (ERR) G_c. In [2] PFM was implemented for humerus fracture based on CT images and an assumed power-law correlation between the heterogeneous G_c and Young modulus.

The first objective is to provide experimental results for cortical bone fracture toughness in the transverse direction to evaluate its distribution in the bone cortex and to investigate a correlation with other material properties such as Young modulus or bone density. Because there are no standards to determine bone fracture toughness, we used the standards for metal and for concrete to evaluate the critical Stress Intensity Factor (SIF) K_c with three-point bending test setup. These standards have been used in previous work ([3,4]) but we also use DIC to estimate the Crack Opening Displacement involved in K_c determination. First bone specimens have been prepared and tested; the results obtained with the different standards are ranged from 2 to 9 MPa√m. Although it is consistent with the previous work ([3,4]), we need to improve the specimen preparation and the DIC procedure. Also, we need to test specimens from a bone which has been scanned at the hospital to compare K_c values with density and Young modulus. 

During my secondment at Sorbonne Université, I will be focus on numerical aspects of PFM using FEniCs with 2 objectives:

1) PF simulation on 3D bone mesh using the code implemented by ESR-5. The results should be compared to the ones obtain from DIC and other FE simulations performed by previous PhD students in our lab at Tel Aviv University. 
2) PF simulation of small specimens three-point bending testing similar to the experiments that I performed at TAU. The aim is to observe the influence of a very small difference in the loading point location on the crack path. Such a difference may appear during the experiments on bone specimens. The results could be also compare to the ones from DIC. 

    
```{figure} ../../esrs/esr8/humerus_mesh.png
:name: image
:height: 200px
3D mesh of humerus obtained from CT images.
```

The numerical work is in progress,  additional preliminary codes will be available soon.





**Objectives**

1.	Formulation of the PFM for an isotropic material with an inhomogeneous Gc
    1.	How does one determine experimentally an inhomogeneous Gc
    2.	Given a microstructure (for example microCT) – how would one determine the macroscopic inhomogeneous Gc based on the microstructural data
    3.	Design experiments to measure in a cortical bone the inhomogeneous Gc – both macroscopic and microscopic. An inhomogeneous Young modulus in bone is a function of the bone density – is Gc the same?
2.	For a homogeneous but anisotropic material, Gc can depend on the direction of crack propagation. There are works on the topic – a literature survey is required.
    1.	What is available for the cortical bone tissue? 
    2.	Design experiments to determine these different Gcs in the various directions. 
    3.	How can one use CT/microCT scans to determine these different Gcs in the various directions? 
3.	Crack nucleation at a surface of the bone – does Gc play a role – there is no crack yet. 
4.	Implement all methods into a high order FE code to allow prediction of fracture in a human long bone.
5.	Perform experiments on bones to validate the models.

## References:

[1] Z. Yosibash, R. Plitman Mayo, G. Dahan, N. Trabelsi, G. Amir, C. Milgrom. 2014. Predicting the stiffness and strength of human femurs with real metastatic tumors. Bone, volume 69: pages 180-190

[2] R. Shen, H. Waisman, Z. Yosibash, G. Dahan. 2019. International Journal for Numerical Methods in Biomedical Engineering, volume 35.

[3] P. Zioupos, J.D Currey. 1998. Changes in the Stiffness, Strength, and Toughness of Human Cortical Bone With Age. Bone, volume 22: pages 57-66

[4] A. Carpinteri, F. Berto, G. Fortese, C. Ronchei, D. Scorza, S. Vantadori. 2017. Modified two-parameter fracture model for bone. Engineering Fracture Mechanics, volume 174: pages 44-53


## Codes

