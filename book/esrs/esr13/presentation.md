# ESR 13

Authors: Arturo Chao Correas

## Short biography

Hola! My name is Arturo and I am from Toledo (Spain). After living in many different places along and across Spain (and even in two different continents!), I went to Seville to study a BSc in Aerospace Engineering. Once finished, I then moved to the UK to pursue an MSc in Advanced Lightweight and Composites Structures at Cranfield University. During these years, I have been working in different research projects, some of these for Airbus or for my former Formula Student Team, and mainly concerning composite materials. These experiences eventually led me to my current PhD position at Politecnico di Torino, where I am part of the DISEG department, working on the development of models for fracture under dynamic regimes.

Below you can see my actual face!

```{figure} ../../esrs/esr13/Photo2.jpeg
:name: label13
:height: 100px
caption
```
## Ph.D. thesis subject: Phase Field and Finite Fracture Mechanics for dynamic crack propagation and delamination in brittle materials and composites

In spite of the huge amount of studies carried out and the many advancements achieved in the last decades in the field of dynamic crack propagation, some aspects still lack of a comprehensive interpretation, such as, for instance, the strengthening and toughening mechanisms, and the phenomenon of crack branching.

In this context, the contribution of the present project consists in the development of numerical and analytical approaches that are robust and devoid of the flaws typical of other methodologies, like the mesh dependency for the cohesive method and the difficulty to manage crack branching and coalescence for the X-FEM.

Such a goal will be pursued by following two complementary strategies: on the one hand, a numerical approach based on the Phase Field model will be developed for a more detailed study of fragmentation of materials subjected to high strain rates. A special interest is in the coupling of PF with the cohesive method in order to get the most advantages in modelling composite materials. On the other hand, the Finite Fracture Mechanics will be extended to dynamics in order to provide a useful tool for preliminary sizing of materials and structures, limiting the use of computationally expensive approaches to the final stage in the structural design, while preserving a physical insight into the fracture mechanics problem due to semi-analytical relations.

Besides contributing to answer to fundamental questions, this project is oriented to develop tools for practical applications in the field of civil engineering and manufacturing industry, where there is the need to optimize material microstructure and products’ shape in order to improve their performance in the dynamic regime. A special emphasis will be put on the additive manufacturing technology, since it makes possible to create very complex microstructured materials showing unprecedented behaviors in the dynamic regime.


## Codes

- `serr-baseline`
- `Elasdodynamics`
- `Explicit-Elastodynamics`
- `Elastodynamics_Damage`
- `Explicit-Elastodynamics_Damage`


## PhD publications

Chao Correas, A., Corrado, M., Sapora, A., & Cornetti, P. (2021). Size-effect on the apparent tensile strength of brittle materials with spherical cavities. Theoretical and Applied Fracture Mechanics, 103120. https://doi.org/10.1016/j.tafmec.2021.103120

Chao Correas, A., Cornetti, P., Corrado, M., Sapora, A. (2023). Finite Fracture Mechanics extension to dynamic loading scenarios. International Journal of Fracture 239, 149–165. https://doi.org/10.1007/s10704-022-00655-x

Ferrian, F., Chao Correas, A., Cornetti, P., Sapora, A. (2023). Size effects on spheroidal voids by Finite Fracture Mechanics and application to corrosion pits. Fatigue and Fracture of Engineering Materials and Structures 46, 875–885. https://doi.org/10.1111/ffe.13902

Chao Correas, A., Sapora, A., Reinoso, J., Corrado, M., Cornetti, P. (2023). Coupled versus energetic nonlocal failure criteria: A case study on the crack onset from circular holes under biaxial loadings. European Journal of Mechanics - A/Solids, 105037. https://doi.org/10.1016/j.euromechsol.2023.105037


## Other publications

Chao Correas, A., Casares Crespo, A., Ghasemnejad, H., & Roshan, G. (2021). Analytical Solutions to Predict Impact Behaviour of Stringer Stiffened Composite Aircraft Panels. Applied Composite Materials, 28(4), 1237–1254. https://doi.org/10.1007/s10443-021-09909-8

Chao Correas, A., Ghasemnejad, H. (2022). Analytical Development on Impact Behaviour of Composite Sandwich Laminates by Differentiated Loading Regimes. Aerospace Science and Technology, 107658. https://doi.org/10.1016/j.ast.2022.107658
