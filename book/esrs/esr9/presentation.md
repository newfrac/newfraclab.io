# ESR 9

Authors: Zeng Liu

## Short biography

My name is Zeng Liu.  I am now a PhD student at NewFrac Innovative Training Network (ITN) - supported by the Marie Skłodowska Curie Actions (MSCA) of European Commission at IMT School for Advanced Studies Lucca (Italy) and University of Seville (Spain).

## Ph.D. thesis subject

I am working on the multi-field and multi-scale modeling of fracture for renewable energy applications during my PhD study. My PhD theis is mainly about the durability and recycling analysis of photovoltaic laminates by the development of novel computational methods. It consists of three parts.

```{figure} ../../esrs/esr9/solidshell.png
:name: image91
:height: 200px
Definition of the cracking shell body and phase field approach for fracture modeling
```

The first part is about the global-local phase field modeling of silicon cell cracking in photovoltaic laminates. To model the very thin silicon cell, the solid shell element at finite deformation is formulated, and both the Enhanced Assumed Strain method and Assumed Natural Strain method are adopted for the alleviation of lockings. Aiming at tackling the poor convergence performance of standard Newton monolithic scheme, a quasi-Newton algorithm is adopted to solve the coupled phase field-displacement governing equations incorporating solid shell formulation in a monolithic manner. The robustness of this quasi-Newton monolithic scheme for the solution of multi-field variational formulation is demonstrated through several paradigmatic boundary value problems, e.g. see below the prediction of fatigue cracking.  

```{figure} ../../esrs/esr9/fatsubres.png
:name: image92
:height: 200px
The contour plot of phase field variable in fatigue modeling with solid shell element formulation solved by quasi-Newton monolithic scheme
```

The second part is about the multiphysics modeling for the durability analysis of photovoltaic laminates. A three-dimensional hygro-thermo-mechanical computational framework for photovoltaic (PV) laminates as well as its numerical implementation are established. Given the difference between the time scales of moisture diffusion and thermo-mechanical problems, a staggered scheme is proposed for the solution of the coupled hygro-thermo-mechanical governing equations. Specifically, the relative displacement and temperature fields are firstly solved from the thermo-mechanical analysis, and then projected to the FE model of moisture diffusion to determine the diffusion coefficient for its subsequent analysis. The proposed method is applied to the simulation of three international standard tests of PV modules, namely the damp heat test, the humidity freeze test, and the thermal cycling test, and numerical predictions are compared with analytical solution for the damp heat case with a constant temperature boundary condition, as well as experimental electroluminescence (EL) images obtained from the thermal cycling test with the cyclic temperature boundary condition. Please see below the prediction of degradation phenomena. 

```{figure} ../../esrs/esr9/tcmoisture.JPG
:name: image93
:height: 200px
The contour plot of moisture diffusion along the EVA layer above the silicon solar cell layer during the thermal cycling simulation.
```

The third part is about the modeling and simulation of the recycling process for end-of-life photovoltaic laminates. To investigate the adhesion degradation of PV laminates due to humidity ingress and temperature effects, a modeling framework coupling the cohesive zone model with a humidity dose model was proposed in this study. A 3D interface finite element considering large deformation including both geometrical and material nonlinearity is also established to accurately simulate the interfacial failure in the 90-degree peeling test. The corresponding FE implementation is based on the variational form of interface contribution to the Principle of Virtual Work of the whole mechanical system, and its subsequent consistent linearization considering large deformation is also detailed. To validate the modeling strategy, peeling between the backsheet and the encapsulant layer (EVA) has been simulated to assess the adhesion strength at different hygrothermal environments after certain exposure duration. 

```{figure} ../../esrs/esr9/pic.png
:name: image94
:height: 200px
A sketch of schematic and kinematic definitions of the interface between two bodies along the deformation process
```
```{figure} ../../esrs/esr9/ODB.png
:name: image95
:height: 200px
Displacement contour in the peeling direction along the simulation process
```
Another approach to recycle the end-of-life photovoltaic laminates is the solvent method. The key problem is the nondestructive recovery of precious silicon wafers for the manufacturing of new products. However, the attempt to comprehensively understand the polymer–solvent system in the PV recycling process is completely lacking. In this work, a thermodynamically consistent large-deformation theory is proposed to model the coupled behavior of this system. The development of continuum theory accounts for the solvent permeation, swelling and elastic deformation, as well as shrinking effects due to the initial crosslinking of ethylene-co-vinyl acetate (EVA). The crosslinking of EVA influences the stiffness of the polymer network, and interacts with the diffusive kinetics of solvents. Also, given the effects of mechanical constraint, the two-way coupling between the EVA deformation and solvent diffusion is established on the basis of thermodynamic arguments.

```{figure} ../../esrs/esr9/pv.png
:name: image96
:height: 200px
Finite element model of a quarter of the complete PV laminate.
```
```{figure} ../../esrs/esr9/damage.PNG
:name: image97
:height: 200px
The damage plots of the silicon cell layer in the three different solvents Toluene, Tetrahydrofuran, and Octane after (a) 5 h, (b) 10 h, and (c) 15 h.
```

## Codes

The developed codes will be made public soon.

## PhD publications 

1.	Zeng Liu, Jose Reinoso, and Marco Paggi. Hygro-thermo-mechanical modeling of thin-walled photovoltaic laminates with polymeric interfaces. Journal of the Mechanics and Physics of Solids, 2022, 169, p.105056.
https://doi.org/10.1016/j.jmps.2022.105056
2.	Zeng Liu, Jose Reinoso, and Marco Paggi. Phase field modeling of brittle fracture in large-deformation solid shells with the efficient quasi-Newton solution and global–local approach. Computer Methods in Applied Mechanics and Engineering, 2022, 399, p.115410.
https://doi.org/10.1016/j.cma.2022.115410
3.	Zeng Liu, Jose Reinoso, and Marco Paggi. A humidity dose-CZM formulation to simulate new end-of-life recycling methods for photovoltaic laminates. Engineering Fracture Mechanics, 2022, 259, p.108125.
https://doi.org/10.1016/j.engfracmech.2021.108125
4.	Zeng Liu, Pietro Lenarda, Jose Reinoso, and Marco Paggi. A multifield coupled thermo-chemo-mechanical theory for the reaction-diffusion modeling in photovoltaics. International Journal for Numerical Methods in Engineering, 2023.
http://doi.org/10.1002/nme.7233
5.	Zeng Liu, Michele Marino, Jose Reinoso, and Marco Paggi. A continuum large-deformation theory for the coupled modeling of polymer-solvent system with application to PV recycling. International Journal of Engineering Science, 2023, 187, p.103842.
https://doi.org/10.1016/j.ijengsci.2023.103842


