# ESR 5

Authors: Camilla Zolesi 


## Short biography

Hi! I am Camilla. I have studied mechanical engineering at La Sapienza, University of Rome (Italy) and I have obtained a double master's degree in Computational Mechanics from La Sapienza and Sorbonne Université (Paris, France). I am now a PhD student at Sorbonne Université, Institut JLR d'Alembert, within the framework of ITN Newfrac. I have a strong interest in Continuum Mechanics, especially in the field of nonlinear behaviour of solids related to damage mechanics.

## Ph.D. thesis subject: Nucleation and propagation of compressive cracks

The phase-field approach to brittle fracture is based on the following minimization problem:

$$     \min_{\boldsymbol{u}, \alpha} \mathcal{E}_{tot}(\boldsymbol{u},\alpha) =  \int_{\Omega}\left[\varphi(\boldsymbol{\varepsilon(\boldsymbol{u})},\alpha) + \psi_{\text{diss}}(\alpha,\boldsymbol{\nabla}\alpha)\right]d\Omega \hspace{10mm}\text{with} \hspace{5mm}
     \varphi(\boldsymbol{\varepsilon},\alpha) = a(\alpha)\varphi_D(\boldsymbol{\varepsilon})  + \varphi_R(\boldsymbol{\varepsilon})$$

where $\mathcal{E}_{tot}$ is the total energy functional, $\boldsymbol{u},\alpha,\boldsymbol{\varepsilon(\boldsymbol{u})}$ are respectively the displacement and damage fields and
the strain tensor, and $\varphi$ and $\psi_{\text{diss}}$ are respectively the elastic and the dissipated energy density. The
minimization problem is solved under the irreversibility constraint for the damage field. The elastic energy
density is split into a damageable part ($\varphi_D$) and a residual part ($\varphi_R$), which serves the double purpose of
avoiding interpenetration of the crack surfaces under compression, and reflecting the physical asymmetry of
fracture behaviour between tension and compression.
Recent results have proved that phase-field approaches can quantitatively predict crack nucleation for mode-I
loading [1]. However, the prediction of crack nucleation under multiaxial loading is crucially influenced by the
energy decomposition, and currently available decompositions (e.g. those in [2,3]) are insufficiently flexible [4]
as they do not allow to reproduce the experimentally measured tensile and compressive (or shear) strengths.
To solve the issue, a novel decomposition was proposed in [4], giving a parametric strength surface à la
Drucker-Prager to be adjusted based on the experimentally measured tensile and compressive strength.

The new theoretical model is implemented numerically to verify the analytical nucleation
curves. Moreover, the analysis is extended beyond nucleation to analyse the localisation modes under
multiaxial loading and to study the propagation behaviour. This implies dealing with the non-linearities
introduced by the new model and with bad conditioning and locking issues related to the corresponding linear
system.


```{figure} ../../esrs/esr5/crack.png
:name: image-compression-esr5
:height: 200px
Damage field under compression using the new model [4].
```
## Codes 
I develop a code to solve gradient-damage problems with dolfinx. You will find further informations [here](codes:damage-dolfinx).

## References:

[1] E. Tanné, T. Li, B. Bourdin, J.-J. Marigo, C. Maurini. 2018. Crack nucleation in variational phase-field models
of brittle fracture. J. Mech. Phys. Solids, 110: 80-99. [doi.org/10.1016/j.jmps.2017.09.006](https://doi.org/10.1016/j.jmps.2017.09.006)

[2] H. Amor, J.-J. Marigo, C. Maurini. 2009. Regularized formulation of the variation brittle fracture with
unilater contact: numerical experiments. J. Mech. Phys. Solids, 57: 1209–1229. [doi.org/10.1016/j.jmps.2009.04.011](https://doi.org/10.1016/j.jmps.2009.04.011)

[3] F. Freddi, G. Royer-Carfagni. 2010: Regularized variational theories of fracture: a unified approach. J. Mech.
Phys. Solids, 58: 1154–1174. [doi.org/10.1016/j.jmps.2010.02.010](https://doi.org/10.1016/j.jmps.2010.02.010)

[4] L. De Lorenzis, C. Maurini. 2021. Nucleation under multi-axial loading in variational phase-field models of
brittle fracture. Int. J. Frac. [doi.org/10.1007/s10704-021-00555-6](https://doi.org/10.1007/s10704-021-00555-6)

