# ESR 12

Juan Macías

## Short biography

My name is Juan Macías, I am from Colombia. I’m a PhD student at the University of Porto (Portugal). I obtained my bachelor's degree in mechanical engineering at the National University of Colombia, Medellín Campus, after an internship at Teams material testing lab in Seville. Then I obtained a master's degree in mechanics of materials and structures from the University of Girona in Spain after an internship of eight months at the University of Strathclyde in Glasgow. My master thesis was about modeling the gradual failure of hybrid composite materials using finite element analysis and cohesive zone models. I have been interested in the study of the failure of aeronautical materials and structures since I started working on the topic during my first internship, I think being part of NewFrac is one of the greatest opportunities to keep working in this field.

## Ph.D. thesis subject

Title: Fracture in fibre-reinforced thermoplastics across scales.

The advent of new manufacturing capabilities for manufacturing long fibre reinforced polymers (LFRP) composites has introduced the possibility of producing highly efficient fibre-reinforced thermoplastics (FRTPs). Due to their inherent characteristics, thermoplastic matrices can overcome the brittle character and difficult recyclability of thermoset matrices. These aspects are of special interest in industrial applications since they encompass higher strain-to-failure, higher fracture toughness and damage tolerance and the ability to be reshaped and reused/recycled. However, they also lead to specific challenges for the representation of the inelastic deformation and fracture of coupons or elements manufacture with this class of multilayered composite materials. The objective of this thesis concerns the analysis of fracture in FRTPs across the scales, i.e., from the scale of the constituents (micro-scale level) to the scale of the laminate (meso- /macro-scale level). Micro-scale analyses will provide more comprehensive understanding about the potential sources and sequence of damage (fibre-matrix decohesion, matrix cracking, delamination, among others), as well as the prospective damage propagation paths, with particular focus on the role of the thermoplastic matrix. Specific aspects concerning the behaviour of different thermoplastic matrices, such as visco-elasto-plasticity and ductile fracture, and the interaction with the reinforcing fibres in terms of damage tolerance will be addressed at this scale. A computational micro-mechanics framework will be developed, integrating robust algorithms that generate reinforcement distributions that are materially and statistically equivalent to real microstructures, constitutive models for the different composite constituents and appropriate definitions of the boundary and loading conditions. Due to the complexity of the geometry and of the boundary and loading conditions of this type of analysis, the Phase-Field (PF) approach to fracture provides the right context to tackle any general case. A particularly important contribution will be the integration of a PF approach to ductile fracture with three dimensional visco-elasto-plasticity to simulate the nonlinear response and fracture of the thermoplastic matrix, and the integration of the PF approach to fracture with nonlinear elasticity and strength asymmetry to simulate the response of the reinforcing fibres. In addition, PF and cohesive zone models will be coupled to predict the decohesion between fibres and matrix. Analyses at this scale will allow the study of the effect of the nature, properties and distribution of the constituents and ply size effects in the energy dissipation and damage mechanisms in FRTPs. These aspects will be considered in the formulation of the appropriate constitutive models for the homogenized orthotropic material at the ply level for meso-/macro-scale analyses. Special attention will be devoted to investigating geometrical effects and loading states in specimens with stress concentrations and holes, which are of relevant practical importance in the aeronautical and aerospace industries.

## Principal task of the thesis: 

Task 1: Literature review: fracture modelling and characterization of FRTPs at different scales. 

Task 2: Development of micro-mechanical models for FRTPs. 

Task 3: Development of constitutive models for FRTP composite constituents based on the PF approach to fracture. 

Task 4: Study the effect of the nature, properties and distribution of the constituents and ply size effects in FRTPs. 

Task 5: Formulation, implementation and validation of constitutive models for FRTPs at the ply (meso-/macro-scale) level. 

Task 6: Development of virtual testing tools for FRTPs based on PF in an industrial context.

## Codes

- ...

