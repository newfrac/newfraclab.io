# ESR 2

Simone Sangaletti

## Short biography

I am a PhD student at NewFrac Innovative Training Network (ITN)- funded by the Marie Skłodowska Curie Actions (MSCA), European Commission - at the University of Seville.
Before coming to Spain, I have worked as structural analyst for Leonardo, acquiring knowledge both on numerical and experimental activities related to structural testung and valudation of aeronautical components. 

## Ph.D. thesis subject

I will work on the micro and meso structural optimization of composite materials realized through additive manufaturing.

## Codes

Right now working on a code to automatize the generation of composite materials with different microstructure to study their influence on the crack propagation scenario. You will find it here as soon as it's available!

